#!/usr/bin/env python
'''aggregation_agent.py
    This program:
    - retrieves performance statistics from the graphite server
    - aggregates related performance metrics
    - shows performance over selected time space'''

import logging
from IPython import embed

import json
import sys
from ZenossAPI import ZenossAPI
from CustomGeneratorAPI import CustomGeneratorAPI
from datetime import datetime

########## GLOBALS ###############
#sys.stderr = sys.stdout
z = ZenossAPI()
generator = CustomGeneratorAPI()

LOG_FORMAT = "%(asctime)s   %(levelname)-7s   %(message)s"
LOG_FILE   = '/home/svjethoe/service_performance_monitor/aggregation.log'
LOGGER     = 'aggregation'
LOG_LEVEL  = 10 # debug == all
console_output = True 

##########  CLASSES  #############
class Device():
    '''Store 1 device object including events'''
    def __init__(self, device=None):
        '''Construct new Device object,
        device = dictionary reply from ZenossAPI'''
        self.name   = device['name']
        self.device = device
        self.recent_events = []
        self.get_recent_events()
    def get_recent_events(self):
        '''check if device has recent events
        return all of them in array'''
        max_age = (3600*24*99) # max age of $recent event in seconds
        all_device_events = z.get_events( uid=self.device['uid'] )
        for event in all_device_events:
            lasttime = datetime.strptime(event['lastTime'],
                    '%Y-%m-%d %H:%M:%S')
            age = datetime.now()-lasttime
            if age.total_seconds() < max_age:
                self.recent_events.append(event)
    def __repr__(self):
        return 'Device( %s )' % self.name
    def __str__(self):
        return self.name
class Customer():
    '''Store customer data
    - Name
    - List of devices
    - List of events / notifications'''
    def __init__(self, customer=None):
        '''Construct new Customer object,
        customer = customer name in Zenoss /Locations/'''
        self.name=customer
        self.devices=[]
    def update_devices(self):
        '''store devices of this customer by name'''
        devices = z.get_devices('/zport/dmd/Locations/%s' % self.name)
        for d in devices:
            device_obj=Device(device=d)
            self.devices.append(device_obj)
    def get_devices_with_recent_events(self):
        '''return a list of devices which have had an event 
        in the last Device[max_age]'''
        devices_with_events = []
        for d in self.devices:
            if d.recent_events:
                devices_with_events.append(d)
        return devices_with_events
    def __repr__(self):
        return 'Customer( %s )' % self.name
    def __str__(self):
        return self.name
class View():
    '''create html view containing customer events'''
    def __init__(self):
        self.header = '''Content-Type: text/html\n
            <!DOCTYPE html>
            <html>
                <head>
                <title>zenoss_to_clearvale</title>
                </head>
                <body>'''
        self.close = '''
                </body>
            </html>'''
        self.body = {}
    def print_device_status_overview(self):
        print "The following devices are affected:<br /><br />"
        print '''<table border="1">
            <caption>device status overview</caption>
            <th>Device</th><th>State</th><th>Events</th>'''
        for d in self.body['invalid_devices']:
            print "<tr><td>%s</td><td>%s</td><td>%s</td></tr>" % (
                d.name,
                d.device['productionState'],
                d.device['events']['critical']['count'] )
        print "</table><br />"
    def print_event_status_overview(self):
        print "The following events have occurred recently:<br /><br />"
        for d in self.body['invalid_devices']:
            print '''<table border="1">
                <caption>event status overview of %s</caption>
                <th>Event</th><th>Device</th><th>Summary</th>''' % d.name
            for e in d.recent_events:
                print "<tr><td>%s</td><td>%s</td><td>%s</td></tr>" % (  
                    e['eventClass']['uid'],
                    d.name,
                    e['summary'],
                    )
            print "</table><br />"
    def print_HTML(self):
        print self.header
        print '''<h1>%s</h1>
            You have %s devices<br />
            Currently %s devices have degraded performance<br />''' % (
                self.body['customer'],
                self.body['total_devices'],
                len(self.body['invalid_devices'])
            )
        self.print_device_status_overview()
        self.print_event_status_overview()
        print self.close
########## FUNCTIONS #############
def set_logging(LOG_FORMAT,LOGFILE,LOGGER,LOG_LEVEL,console_output=True):
    ''' Set logging options and return logger '''
    #logging.basicConfig(format=LOG_FORMAT)
    logger = logging.getLogger(LOGGER)
    logger.setLevel(LOG_LEVEL)
    #### logging filehandle conf ###
    log_file_handle = logging.FileHandler ( LOG_FILE )
    logger.addHandler(log_file_handle)
    log_file_handle.setFormatter(logging.Formatter(LOG_FORMAT) )
    logger.propagate = console_output # no console output if set
    # log.critical("effective loglevel: %s" % (logging.getLevelName(log.getEffectiveLevel())))
    return logger

##########  MAIN   ###############
if __name__ == "__main__":
    log = set_logging(LOG_FORMAT,LOG_FILE,LOGGER,LOG_LEVEL,console_output)
    v = View()                  # store and print output
    if sys.stdin.isatty():
        log.info("######## start interactive ####")
        c = Customer('Amac') # of $1
        c.update_devices()
        v.body['customer']        = c.name
        v.body['total_devices']   = len (c.devices)
        v.body['invalid_devices'] = c.get_devices_with_recent_events()
        v.print_HTML()
    else:
        log.info("######## start batch mode  ####")
        reply = json.loads(sys.stdin.read()) # read json POST into dict
        log.debug ("%s" % reply.items() )
        for key, value in reply.items():
            log.info ("%s\t%s" % (key, value))
        v.print_HTML()
    log.info("############ end   ############")

def ipython():
    '''shortcut ipython'''
#    from ZenossAPI import ZenossAPI
#    z = ZenossAPI()
#    from zenoss_monitor import *
#    events = z.get_events()
    amac = Customer('Amac')
    amac.update_devices()
    amac.print_events()
