#!/usr/bin/python
'''service_agent.py
    - this program generates random service statistics
    - and pushes them to the graphite server on localhost
'''

import numpy
import sys
import time
from socket import socket

CARBON_SERVER = '127.0.0.1'
CARBON_PORT   = 2003

##########  MAIN   ###############
if __name__ == "__main__":
    sock = socket()
    try:
        sock.connect( (CARBON_SERVER,CARBON_PORT) )
    except:
        print "Couldn't connect to %(server)s on port %(port)d, is carbon-agent.py running?" % { 'server':CARBON_SERVER, 'port':CARBON_PORT }
        sys.exit(1)

    # set start time to 30 days ago
    date = time.time()-(30*24*60*60)
    now  = int( date )

    # generate 30 days of random data
    for i in xrange(30*24*60): 
        # simulate binary distributed performance component
        # binomial distributed with
        # p = 0.9
        binary  = numpy.random.binomial(n=1, p=0.9) 

        # simulate continuous distributed performance component
        # normally distributed with
        # mean  = 190
        # stdev = 20
        continuous = numpy.random.normal(loc=190,scale=20)


        lines = []
        lines.append("customer.service.binary_com  %s %d" % \
            (binary,now))
        lines.append("customer.service.continuous_com %s %d" % \
            (continuous,now))
        #all lines must end in a newline
        message = '\n'.join(lines) + '\n' 
        sock.sendall(message)

        # simulate report every minute
        now  += 60 

        # safety sleep
        time.sleep(0.0001)
