#!/usr/bin/env python
'''aggregation_agent.py
    This program:
    - retrieves performance statistics from the graphite GRAPHITE_SERVER
    - aggregates related performance metrics
    - shows performance over selected time space
    
    Sudesh Jethoe
    sudesh@jethoe.nl
    2013-03-12
    '''

import urllib
import urllib2
import json
import sys
import numpy
import scipy

from datetime import datetime
from IPython import embed

########## GLOBALS ###############
GRAPHITE_SERVER = 'localhost'

########## FUNCTIONS #############
def request(request_data):
    '''compile url, execute request, parse request,
    filter request data, return numpy array in which
    [0] = y (actual performance of service component)
    [1] = x (unix_time)'''
    request_data['format'] = 'json'
    url = "http://%s/render?" % GRAPHITE_SERVER
    querystring = urllib.urlencode(request_data)
    # print url+querystring
    reply = \
        json.loads(urllib2.urlopen(url+querystring).read())[0]['datapoints']

    # filter out 'None' data
    reply  = [ row for row in reply if not row[0] == None ]

    # convert to numpy array for easy slicing
    reply = numpy.array(reply)
    return reply

def graph_url(request_data):
    '''construct url for graph'''
    url = "http://%s/render?" % GRAPHITE_SERVER
    formatting = {
        'format'    : 'png',
        'vtitle'    : 'performance',
        'minXStep'  : 2,
        'lineMode'  : 'slope',
        'areaMode'  : 'first',
        'hideLegend': 'True',
    }
    request_data.update(formatting)
    querystring = urllib.urlencode(request_data)
    return url+querystring
        
def calc_integral(y,x=[]):
    '''approximate integral by using Riemann middle sum
    this function is not actually used, but describes somewhat
    how the auc (area under curve) is calculated'''
    auc = 0
    for i in range(1,len(y)):
        y_avg  = (y[i]+y[i-1])/2
        d_x  = x[i]-x[i-1]
        auc  += d_x*y_avg
    return auc
    
##########  MAIN   ###############
if __name__ == "__main__":
    request_data = {}
    request_data['from']   = '-30d' # 30 days
    request_data['until']  = 'today'

    # retrieve performance of service components 
    request_data['target'] = 'customer.service.continuous_com'
    continuous_act_perf = request ( request_data )

    request_data['target'] = 'customer.service.binary_com'
    binary_act_perf = request ( request_data )

    # time_space readability var:
    time_space = binary_act_perf[:,1]

    # define mutually agreed performance 
    BINARY_MAP    = .8
    CONTINUOUS_MAP= 200
    # define weights
    # binary_w + continuous_w should be 1!
    BINARY_WEIGHT     = .2
    CONTINUOUS_WEIGHT = .8
    
    # convert P(Int Actual Perf) to
    # ( P(Int Actual Perf/P(MAP) )^w
    binary_weighted_performance = \
        (binary_act_perf[:,0]/BINARY_MAP)**BINARY_WEIGHT
    continuous_weighted_performance = \
        (continuous_act_perf[:,0]/CONTINUOUS_MAP)**CONTINUOUS_WEIGHT

    # combine weights by multiplicating them
    combined_performance = \
        binary_weighted_performance * continuous_weighted_performance

    # achieved performance approximation with trapezoidal rule
    auc = scipy.trapz(combined_performance,x=time_space)
    # convert auc from secs to hrs
    auc /= 3600
    print "%s is the unweighted total performance for this service over the time space" % auc
    # calculate weighted total performance
    total_performance = auc/len(time_space)
    print "%s is the weighted total performance" % total_performance
    if total_performance < 1 :
        print "The mutual agreed performance has not been met
            within the agreed time space"
    else:
        print "The mutual agreed performance has been met
            within the agreed time space"
    embed()

