# Zenoss-4.x JSON API (python)
#
# >>> z = ZenossAPI()
# >>> events = z.get_events()

import json
import urllib
import urllib2
import sys
import logging

########## GLOBALS ###############
sys.stderr = sys.stdout
log = logging.getLogger('perfmon')

with open('config.json') as file:
    config = json.load(file)

ZENOSS_INSTANCE = config['ZENOSS_INSTANCE']
ZENOSS_USERNAME = config['ZENOSS_USERNAME']
ZENOSS_PASSWORD = config['ZENOSS_PASSWORD']

ROUTERS = { 'MessagingRouter': 'messaging',
            'EventsRouter': 'evconsole',
            'ProcessRouter': 'process',
            'ServiceRouter': 'service',
            'DeviceRouter': 'device',
            'NetworkRouter': 'network',
            'TemplateRouter': 'template',
            'DetailNavRouter': 'detailnav',
            'ReportRouter': 'report',
            'MibRouter': 'mib',
            'TriggersRouter': 'triggers',
            'ZenPackRouter': 'zenpack' }

########## CLASSES ###############
class ZenossAPI():
    def __init__(self, debug=False):
        '''Initialize the API connection, log in, and store authentication cookie'''
        # Use the HTTPCookieProcessor as urllib2 does not save cookies by default
        self.urlOpener = urllib2.build_opener(urllib2.HTTPCookieProcessor())
        if debug: self.urlOpener.add_handler(urllib2.HTTPHandler(debuglevel=1))
        self.reqCount = 1

        # Contruct POST params and submit login.
        loginParams = urllib.urlencode(dict(
                        __ac_name = ZENOSS_USERNAME,
                        __ac_password = ZENOSS_PASSWORD,
                        submitted = 'true',
                        came_from = ZENOSS_INSTANCE + '/zport/dmd'))
        self.urlOpener.open(ZENOSS_INSTANCE + '/zport/acl_users/cookieAuthHelper/login',
                            loginParams)
    def _router_request(self, router, method, data=[]):
        if router not in ROUTERS:
            raise Exception('Router "' + router + '" not available.')

        # Contruct a standard URL request for API calls
        req = urllib2.Request(ZENOSS_INSTANCE + '/zport/dmd/' +
                              ROUTERS[router] + '_router')

        # NOTE: Content-type MUST be set to 'application/json' for these requests
        req.add_header('Content-type', 'application/json; charset=utf-8')

        # Convert the request parameters into JSON
        reqData = json.dumps([dict(
                    action=router,
                    method=method,
                    data=data,
                    type='rpc',
                    tid=self.reqCount)])

        # Increment the request count ('tid'). More important if sending multiple
        # calls in a single request
        self.reqCount += 1
        # Submit the request and convert the returned JSON to objects
        reply = json.loads(self.urlOpener.open(req, reqData).read())
        # Check if reply successfull and return data
        if reply['result']['success']:
            return reply['result']
        else:
            log.warn('# request failed, invalid reply, received:\n%s' % reply['result']['success'])
            print reply['result']['msg']
            exit(1)
    def get_triggers(self, deviceClass='/zport/dmd/Devices'):
        '''return array of triggers'''
        return self._router_request('TriggersRouter', 'getTriggers')['data']
    def get_notifications(self, deviceClass='/zport/dmd/Devices'):
        '''return array of notification items'''
        reply = self._router_request('TriggersRouter', 'getNotifications')['data'][0]
        return reply['content']['items']
    def get_devices(self, uid='/zport/dmd/Devices'):
        '''return array of devices
        to get a specific customer set
        uid="/zport/dmd/Locations/Amac" '''
        data = {'uid': uid,
                'limit': 999,
                'sort':'name',
                'params': {},
                }
        return self._router_request('DeviceRouter', 'getDevices',[data])['devices']
    def get_events( self, uid=None ):
        '''return array of occured events for device(s) in:
        tree uid or device uid
        ex: z.get_events(uid = "/zport/dmd/Locations/$location" '''
        data = dict(start=0, limit=500, dir='DESC', sort='severity')
        data['uid'] = uid
        data['params'] = dict(severity=[5,4,3,2], eventState=[0,1])
        data['keys'] = ['eventState', 'severity', 'component', 'eventClass', 'summary', 'firstTime', 'lastTime', 'count', 'evid', 'eventClassKey', 'message']
        return self._router_request('EventsRouter', 'query', [data])['events']
    def get_locations(self, device=None, component=None, eventClass=None):
        '''return array of occured events'''
        data = dict(start=0, limit=100, dir='DESC', sort='severity')
        data['params'] = dict(severity=[5,4,3,2], eventState=[0,1])
        data['keys'] = ['eventState', 'severity', 'component', 'eventClass', 'summary', 'firstTime', 'lastTime', 'count', 'evid', 'eventClassKey', 'message']

        if device: data['params']['device'] = device
        if component: data['params']['component'] = component
        if eventClass: data['params']['eventClass'] = eventClass

        return self._router_request('EventsRouter', 'query', [data])['events']
